'use strict';

const logger = require('pino')();
const AWS = require('aws-sdk');
const credentials = new AWS.Credentials({accessKeyId: 'test', secretAccessKey: 'test'});
const { Consumer } = require('sqs-consumer');
const scopeType = 'event';
const source = 'sqs';
const queueUrl = 'http://localhost:4566/000000000000/main_queue';
const sqs = new AWS.SQS({endpoint: queueUrl, credentials, region: 'us-east-1'});

const app = Consumer.create({
    sqs,
    queueUrl,
    handleMessage: async (message) => {
        const scopeName = 'handleMessage';
        const child = logger.child({ scopeName, scopeType, source });
        child.info(`The ${scopeType} ${scopeName} was called!`);

        // do some work with `message`
        child.info(message.Body);
    }
});

app.on('error', (err) => {
    const scopeName = 'error';
    const child = logger.child({ scopeName, scopeType, source });
    child.info(`The ${scopeType} ${scopeName} was called!`);
    child.error(err.message);
});

app.on('processing_error', (err) => {
    const scopeName = 'processing_error';
    const child = logger.child({ scopeName, scopeType, source });
    child.info(`The ${scopeType} ${scopeName} was called!`);
    console.error(err.message);
});

app.start();
