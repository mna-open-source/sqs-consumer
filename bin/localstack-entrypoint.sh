#!/usr/bin/env bash
printf "Configuring localstack components..."

readonly LOCALSTACK_URL=http://localstack:4566

sleep 5;

set -x

aws configure set aws_access_key_id test
aws configure set aws_secret_access_key test
echo "[default]" > ~/.aws/config
echo "region = us-east-1" >> ~/.aws/config
echo "output = json" >> ~/.aws/config

aws --endpoint-url=$LOCALSTACK_URL sqs create-queue --queue-name main_queue

aws --endpoint-url=$LOCALSTACK_URL ssm put-parameter --name "/local/arn/queue/main" --value "arn:aws:sqs:us-east-1:000000000000:main_queue" --type "String" --overwrite

set +x

printf "Localstack component configuration finished"
